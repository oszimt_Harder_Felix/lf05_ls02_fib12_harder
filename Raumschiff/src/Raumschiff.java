import java.util.ArrayList;
public class Raumschiff {
/**
 * @author Felix Harder
 * @version 1.0
 * @since 01.06.2022
 */
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	public static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	public ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	/**
	 * Konstruktor ohne Parameter der Klasse Raumschiff
	 */
	public Raumschiff() {
		 
	}
	/**
	 * Konstruktor der Klasse Raumschiff (mit Parameter). Es findet eine Initialisierung der unten gelisteten Attribute statt
	 * @param photonentorpedoAnzahl	= gibt die Anzahl von abschie�baren Photonentorpedos im Raumschiff
	 * @param energieversorgungInProzent = gibt den Status der Energieversorgung in Prozent des Raumschiffes
	 * @param schildeInProzent = gibt den Zustand der Schilde in Prozent des Raumschiffes
	 * @param huelleInProzent = gibt den Zustand der H�lle in Prozent des Raumschiffes
	 * @param lebenserhaltungssystemeInProzent = gibt den Zustand der Lebenserhaltungssysteme in Prozent des Raumschiffes
	 * @param androidenAnzahl = gibt die Anzahl der Androiden im Raumschiff
	 * @param schiffsname = gibt den Namen des Raumschiffes
	 */
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
		
		
	}
	/**
	 * @return photonentorpedoAnzahl = gibt die Anzahl der Torpedos im Raumschiff
	 */
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}
/**
 * @param photonentorpedoAnzahlNeu = setzt die Anzahl der Torpedos im Raumschiff
 */
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahlNeu) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahlNeu;
	}
/**
 * @return energieversorgungInProzent = gibt den Zustand des Raumschiffes in Prozent hinsichtlich Energieversorgung
 */
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}
/**
 * @param energieversorgungInProzentNeu = setzt den Zustand des Raumschiffes in Prozent hinsichtlich Energieversorgung
 */
	public void setEnergieversorgungInProzent(int energieversorgungInProzentNeu) {
		this.energieversorgungInProzent = energieversorgungInProzentNeu;
	}
/**
 * @return schildeInProzent = gibt den Zustand der Schilde des Raumschiffes in Prozent wieder
 */
	public int getSchildeInProzent() {
		return schildeInProzent;
	}
/**
 * @param schildeInProzentNeu = setzt den Zustand der Schilde des Raumschiffes in Prozent
 */
	public void setSchildeInProzent(int schildeInProzentNeu) {
		this.schildeInProzent = schildeInProzentNeu;
	}
/**
 * @return huelleInProzent = gibt den Zustand der H�lle des Raumschiffes in Prozent
 */
	public int getHuelleInProzent() {
		return huelleInProzent;
	}
/**
 * @param huelleInProzentNeu = setzt den Zustand der H�lle des Raumschiffes in Prozent
 */
	public void setHuelleInProzent(int huelleInProzentNeu) {
		this.huelleInProzent = huelleInProzentNeu;
	}
/**
 * @return lebenserhaltungssystemeInProzent = gibt den Zustand der Lebenserhaltungssysteme des Raumschiffes in Prozent
 */
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}
/**
 * @param lebenserhaltungssystemeInProzentNeu = setzt den Zustand der Lebenserhaltungssysteme des Raumschiffes in Prozent
 */
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzentNeu) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzentNeu;
	}
/**
 * @return androidenAnzahl = gibt die Anzahl der Androiden des Raumschiffes 
 */
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}
/**
 * @param androidenAnzahl = liefert die Anzahl der Androiden des Raumschiffes 
 */
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
/**
 * @return schiffsname = gibt den Namen des Raumschiffes 
 */
	public String getSchiffsname() {
		return schiffsname;
	}
/**
 * @param schiffsname = liefert den Namen des Raumschiffes 
 */
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
/**
 * @param neueLadung = Die neue Ladung wird dem Ladungsverzeichnis hinzugef�gt
 */
	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);	
	}

/**
 * @param r = Das Raumschiff r, welches abgeschossen wird
 * Die Methode bekommt einen Parameter �bergeben, welches dann das Ziel des Torpedos ist
 * Falls die Anzahl der Torpedos 0 ist, dann wird eine Nachricht an Alle ausgegeben (Click),
 * ansonsten bekommen alle die Nachricht, dass der Torpedo abgeschossen wurde und die Methode treffer() wird aufgerufen
 */
	public void photonentorpedoSchiessen(Raumschiff r) {
		r = r;
		if (photonentorpedoAnzahl == 0) nachrichtAnAlle("-=*Click*=-");
		else{ photonentorpedoAnzahl--; nachrichtAnAlle("Photonentorpedo abgeschossen!");
			treffer(r);
			}
	}
/**	
 * @param r = Das Raumschiff r, welches abgeschossen wird
 * Die Methode bekommt einen Parameter �bergeben, welches dann das Ziel der Phaserkanone ist
 * Falls die Energieversorgung unter 50% liegt, dann wird eine Nachricht an alle ausgegeben (Click), 
 * ansonsten wird die Energieversorgung um 50% reduziert sowie wird eine Nachricht an Alle ausgegeben, dass die Phaserkanone abgeschossen wurde
 * Die Methode treffer() wird am Ende aufgerufen
 */
	public void phaserkanoneSchiessen(Raumschiff r) {
		r = r;
		if (energieversorgungInProzent < 50) nachrichtAnAlle("-=*Click*=-");
		else{ energieversorgungInProzent -= 50; nachrichtAnAlle("Phaserkanone abgeschossen!");
			treffer(r);
			}
	}
/**
 * @param r = Das Raumschiff r, welches getroffen wurde
 * Die Schilde des getroffenen Raumschiff werden um 50% geschw�cht.
 * Falls die Schilde dann weniger als 0 sind (kaputt), dann wird die H�lle des getroffenen Raumschiffes um 50% geschw�cht
 * Falls die H�lle dann unter 0% (oder direkt 0%) sinkt, dann gehen die Lebenserhaltungssysteme auf 0 und es wid eine Nachricht an Alle ausgegeben
 */
	private void treffer(Raumschiff r) {
		r = r;
		System.out.print(r.getSchiffsname() + " wurde getroffen!");
		r.schildeInProzent -= 50;
		if (r.schildeInProzent <= 0) {
			r.huelleInProzent -= 50;
			r.energieversorgungInProzent -= 50;}
		if (r.huelleInProzent <= 0) {
			r.lebenserhaltungssystemeInProzent = 0;
			r.nachrichtAnAlle("Die Lebenserhaltungssysteme wurden vernichtet");
		}
		
	}
/**	
 * @param message = Nachricht, die an Alle ausgegeben wird
 */
	public void nachrichtAnAlle(String message) {
//		nachrichtAnAlle = message;
		System.out.println(this.schiffsname + ": " + message);
	}
/**	
 * @return broadcastKommunikator = der Broadcast Kommunikator wird zur�ckgegeben
 */
	public static ArrayList<String> eintraegeLogbuchZurueckgeben() {
//		eintraegeLogbuchZurueckgeben = eintraegeLogbuchZurueckgeben;
		return broadcastKommunikator;
	}

	public void photonentorpedosLaden(int anzahlTorpedos) {
//		photonentorpedosLaden = anzahlTorpedos;
	}

	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden) {
//		reparaturDurchfuehren = reparaturDurchfuehren;
	}
/**
 * Der Zustand des Raumschiffes wird mit entsprechener Beschreibung ausgegeben	
 */
	public void zustandRaumschiff() {
//		zustandRaumschiff = zustandRaumschiff;
		System.out.println("Zustand des Raumschiffes " + this.getSchiffsname() + ":");
		System.out.println("Anzahl der Photonentorpedos: " + this.getPhotonentorpedoAnzahl() + 
				"\nZustand der Energieversorgung in Prozent: " + this.getEnergieversorgungInProzent() +
				"\nZustand der Schilde in Prozent: " + this.getSchildeInProzent() +
				"\nZustand der H�lle in Prozent: " + this.getHuelleInProzent() +
				"\nZustand der Leebenserhaltungssysteme in Prozent: " + this.getLebenserhaltungssystemeInProzent() +
				"\nAnzahl der Androiden: " + this.getAndroidenAnzahl());
	}
/**
 * Die Ladungen des Raumschiffes wird mit entsprechender Beschreibung ausgegeben 
 */
	public void ladungsverzeichnisAusgeben() {
//		ladungsverzeichnisAusgeben = ladungsverzeichnisAusgeben;
		System.out.println("Das Raumschiff " + this.schiffsname + " hat folgende Ladung dabei: ");
		for(Ladung temp : ladungsverzeichnis) {
			System.out.println(temp.getBezeichnung() + "! Anzahl: " + temp.getMenge());
			}
	}
	
	public void ladungsverzeichnisAufraeumen() {
//		ladungsverzeichnisAufraeumen = ladungsverzeichnisAufraeumen;
//		int anzahl = 0;
//		if (anzahl == 0){
	}
/**
 * Eine To-String-Methode, die den Zustand des Raumschiffes ausgibt
 */
	public String toString() {
		return "Der Zustand f�r das folgende Raumschiff wird ausgegeben! " + "\n" +
				"Schiffsname: "	+ this.schiffsname  + "\n" + 
				"Photonentorpedoanzahl: " + this.photonentorpedoAnzahl + "\n" +
				"Energieversorgung: " + this.energieversorgungInProzent + "%\n" + 
				"Schilde: " + this.schildeInProzent + "%\n" + 
				"H�lle: " + this.huelleInProzent + "%\n" + 
				"Lebenserhaltungssysteme: " + this.lebenserhaltungssystemeInProzent + "%\n" +
				"Androidenanzahl: " + this.androidenAnzahl + "\n";			
	}
}
	
